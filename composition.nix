{ pkgs, ... }: {
  roles = {
    numpex = { pkgs, ... }:
      let
        spackStoreLocation = "/tmp/spack";
        spack = pkgs.callPackage ./spack.nix { inherit spackStoreLocation; };
      in {
        environment.systemPackages = [ spack ];
        services.guix.enable = true;
      };
  };
  testScript = ''
    foo.succeed("true")
  '';
}
